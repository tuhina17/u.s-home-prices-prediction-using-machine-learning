import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error, mean_squared_error
import numpy as np

# Load the datasets
home_prices = pd.read_excel("/Users/tuhinachaudhari/Documents/HomePriceIndex(Not sesonally).xlsx")
population = pd.read_excel("/Users/tuhinachaudhari/Documents/Population.xlsx")
median_income = pd.read_excel("/Users/tuhinachaudhari/Documents/MedianFamilyIncome.xlsx")

# Checking for missing values and handling them
home_prices.isnull().sum()
population.isnull().sum()
median_income.isnull().sum()

# Indexing by the observation date
home_prices['observation_date'] = pd.to_datetime(home_prices['observation_date'])
home_prices.set_index('observation_date', inplace=True)

population['observation_date'] = pd.to_datetime(population['observation_date'])
population.set_index('observation_date', inplace=True)

median_income['observation_date'] = pd.to_datetime(median_income['observation_date'])
median_income.set_index('observation_date', inplace=True)

# Data Exploration
plt.figure(figsize=(12, 6))
plt.plot(home_prices.index, home_prices['CSUSHPINSA'], label='Home Prices', color='blue')
plt.xlabel('Year')
plt.ylabel('Home Prices')
plt.title('Home Prices Over Time')
plt.legend()
plt.show()

# Merge the datasets into a single DataFrame
merged_df = home_prices.copy()
merged_df['Population'] = population['POPTHM']
merged_df['MedianIncome'] = median_income['MEFAINUSA646N']

print(merged_df)

# Create a lag feature for home prices (shifted by 1 month)
merged_df['PreviousHomePrices'] = merged_df['CSUSHPINSA'].shift(1)

# Drop rows with missing values (first row)
merged_df.dropna(inplace=True)

# Define X (features) and y (target)
X = merged_df[['Population', 'MedianIncome', 'PreviousHomePrices']]
y = merged_df['CSUSHPINSA']

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Create and train a linear regression model
model = LinearRegression()
model.fit(X_train, y_train)

# Make predictions
y_pred = model.predict(X_test)

# Evaluate the model
mae = mean_absolute_error(y_test, y_pred)
rmse = np.sqrt(mean_squared_error(y_test, y_pred))

print("Mean Absolute Error (MAE):", mae)
print("Root Mean Squared Error (RMSE):", rmse)

# Print the coefficients of the model
print("Coefficients:", model.coef_)

# Visualize the model's predictions vs. actual values
plt.figure(figsize=(12, 6))
plt.scatter(y_test.index, y_test, label='Actual', color='blue', s=20)
plt.scatter(y_test.index, y_pred, label='Predicted', color='red', s=20)
plt.xlabel('Year')
plt.ylabel('Home Prices')
plt.title('Actual vs. Predicted Home Prices')
plt.legend()
plt.show()

# Testing for 2022_01_01
data_2022_01_01 = pd.DataFrame({
    'Population': [332918],
    'MedianIncome': [92750],
    'PreviousHomePrices': [282.041]  
})

# Use the trained model to predict home prices for 2022-01-01
predicted_home_price_2022_01_01 = model.predict(data_2022_01_01[['Population', 'MedianIncome', 'PreviousHomePrices']])

# Print the predicted home price for 2022-01-01
print("Predicted Home Price for 2022-01-01:", predicted_home_price_2022_01_01)